﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nancy.RestApi.Models
{
    public class PersonModel
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
    }
}
