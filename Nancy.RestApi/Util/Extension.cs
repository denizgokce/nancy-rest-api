﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nancy.RestApi
{
    public static class Extension
    {
        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static string ToJson(this List<object> obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static int ToInt(this object obj)
        {
            return Convert.ToInt32(obj);
        }
    }
}
