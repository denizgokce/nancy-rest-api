﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nancy.RestApi.Database
{
    [Table("person",Schema ="public")]
    public class Person
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [Column("firstname")]
        public string firstname { get; set; }
        [Column("lastname")]
        public string lastname { get; set; }
    }
}
