﻿using Nancy.ModelBinding;
using Nancy.RestApi.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nancy.RestApi.Modules
{
    public class PersonModule : NancyModule
    {
        public PersonModule(DatabaseContext dbContext)
        {
            Get("/person", args => dbContext.Person.ToList().ToJson());


            Post("/person", args =>
            {
                var personModel = this.Bind<Models.PersonModel>();
                var person = dbContext.Person.Add(new Person()
                {
                    firstname = personModel.firstname,
                    lastname = personModel.lastname
                }).Entity;
                dbContext.SaveChanges();
                return person.ToJson();
            });

            Put("/person/{id}", (args) =>
            {
                int id = (int)args.id;
                var personModel = this.Bind<Models.PersonModel>();
                var person = dbContext.Person.FirstOrDefault(x => x.id == id);
                person.firstname = personModel.firstname;
                person.lastname = personModel.lastname;
                dbContext.SaveChanges();
                return person.ToJson();
            });

            Delete("/person/{id}", (args) =>
            {
                int id = (int)args.id;
                var person = dbContext.Person.FirstOrDefault(x => x.id == id);
                dbContext.Person.Remove(person);
                dbContext.SaveChanges();
                return person.ToJson();
            });
        }
    }
}
