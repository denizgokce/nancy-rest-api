﻿using Nancy.RestApi.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nancy.RestApi.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule(DatabaseContext context)
        {
            Get("/", args => "Hello World");

            Get("/SayHello", args => $"Hello {this.Request.Query["name"]}");

            Get("/SayHello2/{name}", args => $"Hello {args.name}");
        }
    }
}
